import { Injectable } from '@angular/core';

import { NgbDate } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor() { }

  convertDate(date: NgbDate) {
    return new Date(date.year, date.month - 1, date.day + 1, 0, 0, 0);
  }

}
