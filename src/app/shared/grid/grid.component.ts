import { LaptopService } from './../../laptops/services/laptop.service';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GridOptions } from 'ag-grid-community';
import { LaptopDetailComponent } from 'src/app/laptops/laptop-detail/laptop-detail.component';
import { UserDetailComponent } from 'src/app/users/user-detail/user-detail.component';
import { LaptopAddComponent } from 'src/app/laptops/laptop-add/laptop-add.component';
import { UserAddComponent } from 'src/app/users/user-add/user-add.component';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})

export class GridComponent implements OnInit, OnDestroy {

  @Input() rowData: any;
  @Input() columnDefs: any;
  @Input() rowClick: boolean;
  @Input() rowClickData: [];

  laptopsSubscription: Subscription;

  quickSearchValue = '';
  gridOptions = <GridOptions>{};

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private modal: NgbModal,
    private laptopService: LaptopService
  ) { }

  ngOnInit() {

    if(this.rowClick === true) {
      this.gridOptions = {
        ...this.gridOptions,
        onRowClicked: this.onSelectionChanged.bind(this)
      }
    }

    this.route.queryParams
      .subscribe(params => {
        if (params.laptop || params.user) {

          setTimeout(() => { // setTimeout is needed for a Angular bug that throws an 'ExpressionChangedAfterItHasBeenCheckedError' error
            let modalRef;

            if (params.laptop) {
              if(params.laptop == 'add') {
                modalRef = this.modal.open(LaptopAddComponent, {size: 'xl'});
              } else {
                modalRef = this.modal.open(LaptopDetailComponent, {size: 'xl'});
                modalRef.componentInstance.laptopId = parseInt(params.laptop, 10);
              }
            }

            if (params.user) {
              if(params.user == 'add') {
                modalRef = this.modal.open(UserAddComponent, {size: 'xl'});
              } else {
                modalRef = this.modal.open(UserDetailComponent, {size: 'xl'});
                modalRef.componentInstance.userId = parseInt(params.user, 10);
              }
            }

            modalRef.result.then(() => {
              this.clearQueryParams();
              //this.updateGrid();
            }, () => {
              this.clearQueryParams();
              //this.updateGrid();
            });

          });

        }
      });

      this.laptopsSubscription = this.laptopService.$laptopsObs
        .subscribe(data => this.gridOptions.api.setRowData(data));

  }

  onQuickFilterChanged() {
    this.gridOptions.api.setQuickFilter(this.quickSearchValue);
  }

  onSelectionChanged(event: any) {
    const selectedRows = event.api.getSelectedRows();
    this.router.navigate(['./'], { relativeTo: this.route, queryParams: { [this.rowClickData['queryParam']]: selectedRows[0][this.rowClickData['key']] } });
  }

  onCellClicked(event: any) {
    const clickedCell = event.column.colId;

    if (clickedCell === 'person.firstName' || clickedCell === 'person.lastName') {
      this.router.navigate(['./'], { relativeTo: this.route, queryParams: { user: event.data.personId } });
    }

    if (clickedCell === 'laptop.name') {
      if(event.data.laptopId !== undefined) {
        this.router.navigate(['./'], { relativeTo: this.route, queryParams: { laptop: event.data.laptopId } });
      }
    }
  }

  clearQueryParams() {
    this.router.navigate(['./'], { relativeTo: this.route, queryParams: { id: null } });
  }

  // updateGrid() {
  //   console.log('updateGrid !!!');
  //   this.gridOptions.api.setRowData(this.rowData);
  // }

  ngOnDestroy() {
    this.laptopsSubscription.unsubscribe();
  }


}
