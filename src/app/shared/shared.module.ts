import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgGridModule } from 'ag-grid-angular';
import { GridComponent } from './grid/grid.component';
import { ModalDeleteConfirmComponent } from './modal-delete-confirm/modal-delete-confirm.component';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [
    GridComponent,
    ModalDeleteConfirmComponent
  ],
  imports: [
    CommonModule,
    AgGridModule.withComponents([]),
    FormsModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    NgbModule,
    AgGridModule,
    GridComponent,
    ModalDeleteConfirmComponent,
    HttpClientModule
    
  ],
  entryComponents: [ModalDeleteConfirmComponent]
})
export class SharedModule { }
