import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { Person } from '../models/person.model';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  users: Person[];

  clickableStyle = { 'cursor': 'pointer' };

  columnDefs = [
      {headerName: 'Firstname', field: 'firstName', sortable: true, filter: true, cellStyle: this.clickableStyle },
      {headerName: 'Lastname', field: 'lastName', sortable: true, filter: true, cellStyle: this.clickableStyle }
  ];

  constructor(
    private userService: UserService,
    private route: ActivatedRoute
  ) {
    // Prefetch api data with resolver
    route.data.subscribe(data => {
      this.users = data['users'];
    });
  }

  ngOnInit() {

  }

}
