declare var require: any;

import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { Person } from '../models/person.model';
import { Laptop } from 'src/app/laptops/models/laptop.model';
import { LaptopService } from 'src/app/laptops/services/laptop.service';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  public users: Person[];

  constructor(
    private http: HttpClient,
    private laptopService: LaptopService
  ) { }

  getUser(id: number) {
    const userDetail = this.users.find(element => element.id === id);
    return userDetail;
  }

  getUsers() {
    return this.http.get<Person[]>(
			'http://systeembeheerapp.azurewebsites.net/api/Person/GetAll',
			{ observe: 'body' } );
  }

  getUserByLaptop(laptopId: number) {
    const userObj = this.laptopService.laptopsInUse.find(element => element.laptopId === laptopId);

    if(userObj) {
      console.log(userObj);
      return this.users.find(user => user.id === userObj.personId);
    } else {
      return null;
    }
  }

  updateUser(formValue: Person) {
    const index = this.users.findIndex(element => element.id === formValue.id);
    console.log(formValue);
    if(index !== -1) {
      this.users[index] = formValue;
    }
  }

  addUser(formValue: Person) {

  }

  deleteUser(id: number) {
    const index = this.users.findIndex(element => element.id === id);
    this.users.splice(index, 1);
  }

}
