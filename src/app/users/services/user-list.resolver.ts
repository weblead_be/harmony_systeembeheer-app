import {Injectable} from "@angular/core";

import { first, map } from 'rxjs/operators';
import { Resolve } from "@angular/router";
import { Observable } from 'rxjs';

import { Person } from '../models/person.model';
import { UserService } from './user.service';

@Injectable()
export class UserListResolver implements Resolve<Person[]> {

    constructor(private userService: UserService) { }

    resolve():Observable<Person[]> {

      let laptopsInUse;

      if(this.userService.users === undefined) {

        laptopsInUse = this.userService.getUsers()
          .pipe(first())
          .pipe(map(response => {
            this.userService.users = response;
            return response;
          }));

      } else {

        laptopsInUse = this.userService.users;

      }

      return laptopsInUse;

        // return this.userService.getLaptopsInUse().pipe(
        //     first());
    }

}
