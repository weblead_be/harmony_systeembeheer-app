import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserComponent } from './user.component';
import { UserRoutingModule } from './user-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { UserListResolver } from './services/user-list.resolver';
import { UserAddComponent } from './user-add/user-add.component';

@NgModule({
  declarations: [
    UserDetailComponent,
    UserListComponent,
    UserComponent,
    UserAddComponent
  ],
  imports: [
    UserRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ],
  providers: [ UserListResolver ],
  entryComponents: [UserDetailComponent, UserAddComponent]
})
export class UserModule { }
