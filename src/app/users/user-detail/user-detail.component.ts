import { Component, OnInit, Input } from '@angular/core';

import { Person } from '../models/person.model';
import { Laptop } from 'src/app/laptops/models/laptop.model';

import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../services/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDeleteConfirmComponent } from 'src/app/shared/modal-delete-confirm/modal-delete-confirm.component';
import { LaptopService } from 'src/app/laptops/services/laptop.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})

export class UserDetailComponent implements OnInit {

  @Input() userId: number;
  userDetail: Person;
  userUpdateForm: FormGroup;
  laptops: Laptop[];
  laptopInUse: Laptop;

  constructor(
    public activeModal: NgbActiveModal,
    private modal: NgbModal,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private laptopService: LaptopService
  ) { }

  ngOnInit() {

    this.userDetail = this.userService.getUser(this.userId);

    this.laptopInUse = this.laptopService.getLaptopInUseByUser(this.userDetail.id);

    this.userUpdateForm = this.formBuilder.group({
      id: [ { value: this.userDetail.id, disabled: true }, [ Validators.required ]],
      firstName: [ this.userDetail.firstName, [ Validators.required ]],
      lastName: [ this.userDetail.lastName, [ Validators.required ]],
      address: this.formBuilder.group({
        street: [ this.userDetail.address.street, [ Validators.required ]],
        number: [ this.userDetail.address.number, [ Validators.required ]],
        zip: [ this.userDetail.address.zip, [ Validators.required ]],
        city: [ this.userDetail.address.city, [ Validators.required ]]
      }),
      laptop: [ this.laptopInUse ]
    })

    this.laptops = this.laptopService.laptops;

  }

  userUpdateSubmitHandler() {
    const formValue: Person = this.userUpdateForm.getRawValue();

    this.userService.updateUser(formValue);
    this.laptopService.updateLaptopsInUse(formValue);

    this.activeModal.close();
  }

  onDeleteConfirm() {
    let modalRef = this.modal.open(ModalDeleteConfirmComponent);
    modalRef.componentInstance.modalName = 'Delete User';
    modalRef.componentInstance.deleteItemName = this.userDetail.firstName+' '+this.userDetail.lastName;

    modalRef.result.then(result => {
      result === 'confirm' ? this.onDelete() : '';
    }, () => { /* overlay dismiss */ });
  }

  onDelete() {
    this.userService.deleteUser(this.userDetail.id);

    if(this.laptopService.laptopsInUse !== undefined) {
      this.laptopService.deleteUserFromLaptopsInUse(this.userDetail.id);
    }

    this.modal.dismissAll();
  }

}
