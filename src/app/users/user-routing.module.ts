import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdalGuard } from 'adal-angular4';

import { UserListComponent } from './user-list/user-list.component';
import { UserComponent } from './user.component';

import { UserListResolver } from './services/user-list.resolver';
import { LaptopInUseResolver } from '../laptops/services/laptop-inuse.resolver';
import { LaptopResolver } from '../laptops/services/laptop-list.resolver';

const resolvers =  {
  laptops: LaptopResolver,
  laptopsInUse: LaptopInUseResolver,
  users: UserListResolver
}

const routes: Routes = [
    {
        path: 'users',
        component: UserComponent,
        canActivate: [AdalGuard],
        children: [
          {
            path: '',
            redirectTo: 'list',
            pathMatch: 'full'
          },
          {
            path: 'list',
            component: UserListComponent,
            canActivate: [AdalGuard],
            resolve: resolvers
          }
        ]
      }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
