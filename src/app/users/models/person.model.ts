export class Person {
    id: number;
    firstName: string;
    lastName: string;
    address?: Address;
}

export class Address {
  id: number;
  street: string;
  number: string;
  zip: string;
  city: string;
}
