import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Laptop } from 'src/app/laptops/models/laptop.model';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../services/user.service';
import { LaptopService } from 'src/app/laptops/services/laptop.service';
import { Person } from '../models/person.model';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {

  userAddForm: FormGroup;
  laptops: Laptop[];
  laptopInUse: Laptop;

  constructor(
    public activeModal: NgbActiveModal,
    private modal: NgbModal,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private laptopService: LaptopService
  ) { }

  ngOnInit() {

    this.userAddForm = this.formBuilder.group({
      firstName: [ '', [ Validators.required ]],
      lastName: [ '', [ Validators.required ]],
      address: this.formBuilder.group({
        street: [ '', [ Validators.required ]],
        number: [ '', [ Validators.required ]],
        zip: [ '', [ Validators.required ]],
        city: [ '', [ Validators.required ]]
      }),
      laptop: [ '' ]
    })

    this.laptops = this.laptopService.laptops;
  }

  userAddSubmitHandler() {
    const formValue: Person = this.userAddForm.getRawValue();

    this.userService.addUser(formValue);

    this.activeModal.close();
  }

}
