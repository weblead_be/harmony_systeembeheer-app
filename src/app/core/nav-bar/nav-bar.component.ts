import { Component, OnInit } from '@angular/core';

import { AdalService } from 'adal-angular4';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})

export class NavBarComponent implements OnInit {
  showProfileMenu: boolean = false;

  constructor(
    private adalService: AdalService
  ) { }

  ngOnInit() {
    this.adalService.handleWindowCallback();
  }

  login() {
    this.adalService.login();
  }

  logout() {
    this.adalService.logOut();
  }

  get authenticated(): boolean {
    if (!this.adalService.userInfo.authenticated && this.adalService.userInfo.userName) {
      this.adalService.login();
    }
    return this.adalService.userInfo.authenticated;
  }

  toggleProfile() {
    this.showProfileMenu = !this.showProfileMenu;
  }

}
