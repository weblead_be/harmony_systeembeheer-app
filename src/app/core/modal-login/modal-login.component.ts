import { Component, OnInit } from '@angular/core';

import { AdalService } from 'adal-angular4';

@Component({
  selector: 'app-modal-login',
  templateUrl: './modal-login.component.html',
  styleUrls: ['./modal-login.component.scss']
})
export class ModalLoginComponent implements OnInit {

  constructor(private adalService: AdalService) { }

  ngOnInit() {
  }

  login() {
    this.adalService.login();
  }

}
