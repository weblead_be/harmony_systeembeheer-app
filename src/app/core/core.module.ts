import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { RouterModule } from '@angular/router';
import { ModalLoginComponent } from './modal-login/modal-login.component';

@NgModule({
  declarations: [
    NavBarComponent,
    SideBarComponent,
    ModalLoginComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  exports: [
    NavBarComponent,
    SideBarComponent
  ],
  entryComponents: [
    ModalLoginComponent
  ]
})
export class CoreModule { }
