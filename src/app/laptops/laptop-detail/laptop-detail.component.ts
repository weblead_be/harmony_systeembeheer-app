import { Component, OnInit, Input } from '@angular/core';

import { NgbActiveModal, NgbModal, NgbCalendar, NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

import { LaptopService } from '../services/laptop.service';
import { Laptop } from '../models/laptop.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ModalDeleteConfirmComponent } from './../../shared/modal-delete-confirm/modal-delete-confirm.component';
import { Person } from 'src/app/users/models/person.model';
import { UserService } from 'src/app/users/services/user.service';
import { LaptopsInUse } from '../models/laptops-inuse.model';
import { SharedService } from 'src/app/shared/services/shared.service';

@Component({
  selector: 'app-laptop-detail',
  templateUrl: './laptop-detail.component.html',
  styleUrls: ['./laptop-detail.component.scss']
})
export class LaptopDetailComponent implements OnInit {

  @Input() laptopId: number;
  laptopDetail: Laptop;
  laptopUpdateForm: FormGroup;
  users: Person[];
  usedBy: Person;
  usedInfo: LaptopsInUse;
  purchaseDate;
  useStatus: string;
  usePersonId: number = null;
  useFrom;
  useUntil;
  activetab: number = 1;

  constructor(
    public activeModal: NgbActiveModal,
    private modal: NgbModal,
    private laptopService: LaptopService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private sharedService: SharedService
  ) { }

  ngOnInit() {

    this.laptopDetail = this.laptopService.getLaptop(this.laptopId);
    this.purchaseDate = new Date(this.laptopDetail.purchaseDate);
    this.purchaseDate = { day: this.purchaseDate.getDate(), month: this.purchaseDate.getMonth() + 1, year: this.purchaseDate.getFullYear()};

    this.users = this.userService.users;

    this.usedInfo = this.laptopService.getUseInfoByLaptop(this.laptopId);

    // Check and Set the status of usedInfo
    (this.usedInfo === null) ? this.useStatus = 'new' : this.useStatus = 'update';

    if(this.useStatus === 'update') {

      this.useFrom = new Date(this.usedInfo.from);
      this.useFrom = { day: this.useFrom.getDate(), month: this.useFrom.getMonth() + 1, year: this.useFrom.getFullYear()};

      this.useUntil = new Date(this.usedInfo.until);
      this.useUntil = { day: this.useUntil.getDate(), month: this.useUntil.getMonth() + 1, year: this.useUntil.getFullYear()};

      this.usePersonId = this.usedInfo.person.id;
    }

    this.laptopUpdateForm = this.formBuilder.group({
      laptop: this.formBuilder.group({
        id: [ { value: this.laptopDetail.id, disabled: true }, [ Validators.required ]],
        name: [ this.laptopDetail.name, [ Validators.required ]],
        brand: [ this.laptopDetail.brand, [ Validators.required ]],
        type: [ this.laptopDetail.type, [ Validators.required ]],
        serialNumber: [ this.laptopDetail.serialNumber, [ Validators.required ]],
        warantyNumber: [ this.laptopDetail.warantyNumber, [ Validators.required ]],
        serviceTag: [ this.laptopDetail.serviceTag, [ Validators.required ]],
        screenSize: [ this.laptopDetail.screenSize, [ Validators.required ]],
        keyboardLayout: [ this.laptopDetail.keyboardLayout, [ Validators.required ]],
        purchaseDate: [ this.purchaseDate, [ Validators.required ]],
        note: [ this.laptopDetail.note ],
      }),
      inuse: this.formBuilder.group({
        user: [ this.usePersonId ],
        useFrom: [ this.useFrom ],
        useUntil: [ this.useUntil ]
      })
    });
  }

  laptopUpdateSubmitHandler() {
    let formValue = this.laptopUpdateForm.getRawValue();
    console.log('RAW ', formValue)
    formValue.laptop.purchaseDate = this.sharedService.convertDate(formValue.laptop.purchaseDate);

    this.laptopService.updateLaptop(formValue.laptop).subscribe(response => {
      this.updateLaptopList();
      this.laptopService.updateLaptopsInUse(formValue.laptop);
    });


    this.handleLaptopInUseUpdate(formValue);

  }

  handleLaptopInUseUpdate(formValue) {
    let status = this.laptopService.laptopsInUseCheckStatus(this.usedInfo, formValue.inuse);
    console.log(status);
    let laptopInUse;

    //prepare object
    if(status !== 'delete') {
      console.log(formValue);
      laptopInUse = {
        from: this.sharedService.convertDate(formValue.inuse.useFrom),
        until: this.sharedService.convertDate(formValue.inuse.useUntil),
        personId: formValue.inuse.user,
        laptopId: formValue.laptop.id,
        note: ''
      }
    }

    if(status === 'update') this.updateLaptopInUse(laptopInUse);
    if(status === 'add') this.addLaptopInUse(laptopInUse);
    if(status === 'remove') this.removeLaptopInUse(formValue.inuse)

  }

  // add
  addLaptopInUse(laptopInUse) {
    console.log('adding');
  }

  // update
  updateLaptopInUse(laptopInUse) {
    console.log('updating');
    laptopInUse['id'] = this.usedInfo.id;
    this.laptopService.updateLaptopsInUse(laptopInUse).subscribe(response => {
      this.updateLaptopList();
      this.updateLaptopsInUseList();
    });
  }

  // remove
  removeLaptopInUse(laptopInUse) {
    console.log('removing');
  }
  
  updateLaptopList() {
    this.laptopService.getLaptops().subscribe(response => {
      this.laptopService.updateLaptopsList(response); /* update observable */
      this.modal.dismissAll();
    });
  }

  updateLaptopsInUseList() {
    this.laptopService.getLaptopsInUse().subscribe(response => {
      console.log(response);
      this.laptopService.updateLaptopsInUseList(response); /* update observable */
      this.modal.dismissAll();
    });
  }

  onDeleteConfirm() {
    let modalRef = this.modal.open(ModalDeleteConfirmComponent);
    modalRef.componentInstance.modalName = 'Delete Laptop';
    modalRef.componentInstance.deleteItemName = this.laptopDetail.name;

    modalRef.result.then(result => {
      result === 'confirm' ? this.onDelete() : '';
    }, () => { /* overlay dismiss */ });
  }

  onDelete() {
    this.laptopService.deleteLaptop(this.laptopDetail.id).subscribe(response => {
      this.updateLaptopList();
      this.laptopService.deleteLaptopFromLaptopsInUse(this.laptopDetail.id);
    });
  }

}
