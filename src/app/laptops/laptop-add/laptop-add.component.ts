import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { LaptopService } from '../services/laptop.service';
import { Laptop } from '../models/laptop.model';

@Component({
  selector: 'app-laptop-add',
  templateUrl: './laptop-add.component.html',
  styleUrls: ['./laptop-add.component.scss']
})
export class LaptopAddComponent implements OnInit {

  laptopAddForm: FormGroup;

  constructor(
    public activeModal: NgbActiveModal,
    private laptopService: LaptopService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {

    this.laptopAddForm = this.formBuilder.group({
      name: ['', [ Validators.required ]],
      brand: ['', [ Validators.required ]],
      type: ['', [ Validators.required ]],
      serialNumber: ['', [ Validators.required ]],
      warantyNumber: ['', [ Validators.required ]],
      serviceTag: ['', [ Validators.required ]],
      screenSize: ['', [ Validators.required ]],
      keyboardLayout: ['1', [ Validators.required ]],
      purchaseDate: ['', [ Validators.required ]],
      note: ['']
    })

  }

  laptopAddSubmitHandler() {
    let formValue: Laptop = this.laptopAddForm.getRawValue();
    let purchaseDate = formValue.purchaseDate;
    purchaseDate = new Date(purchaseDate.year, purchaseDate.month - 1, purchaseDate.day + 1, 0, 0, 0);
    formValue.purchaseDate = purchaseDate;

    this.laptopService.addLaptop(formValue).subscribe(response => {
        this.laptopService.getLaptops().subscribe(response => {
            this.laptopService.updateLaptopsList(response);
            this.activeModal.close();
        });
    });




  }

}
