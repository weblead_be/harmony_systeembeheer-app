import { Component, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';

import { Laptop } from '../models/laptop.model';
import { LaptopService } from '../services/laptop.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-laptop-list',
  templateUrl: './laptop-list.component.html',
  styleUrls: ['./laptop-list.component.scss']
})
export class LaptopListComponent implements OnInit {

  laptops: Laptop[];

  clickableStyle = { 'cursor': 'pointer' };

  columnDefs = [
      {headerName: 'Name', field: 'name', sortable: true, filter: true, cellStyle: this.clickableStyle },
      {headerName: 'Brand', field: 'brand', sortable: true, filter: true, cellStyle: this.clickableStyle },
      {headerName: 'Type', field: 'type', sortable: true, filter: true, cellStyle: this.clickableStyle },
      {headerName: 'Screen size', field: 'screenSize', sortable: true, filter: true, cellStyle: this.clickableStyle },
      {headerName: 'Keyboard', field: 'keyboardLayout', sortable: true, filter: true, cellStyle: this.clickableStyle },
      {headerName: 'Purchase date', field: 'purchaseDate', sortable: true, filter: true, cellStyle: this.clickableStyle }
  ];

  constructor(
    private laptopService: LaptopService,
    private route: ActivatedRoute
  ) {
    // Prefetch api data with resolver
    route.data.subscribe(data => {
      this.laptops = data['laptops'];
    });
  }

  ngOnInit() {

  }

}
