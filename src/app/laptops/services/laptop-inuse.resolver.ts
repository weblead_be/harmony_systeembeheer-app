import {Injectable} from "@angular/core";

import { first, map } from 'rxjs/operators';
import { Resolve } from "@angular/router";
import { Observable } from 'rxjs';

import { LaptopService } from './laptop.service';
import { LaptopsInUse } from '../models/laptops-inuse.model';

@Injectable()
export class LaptopInUseResolver implements Resolve<LaptopsInUse[]> {

    constructor(private laptopService: LaptopService) { }

    resolve():Observable<LaptopsInUse[]> {

      let laptopsInUse;

      if(this.laptopService.laptopsInUse === undefined) {

        laptopsInUse = this.laptopService.getLaptopsInUse()
          .pipe(first())
          .pipe(map(response => {
            this.laptopService.laptopsInUse = response;
            return response;
          }));

      } else {

        laptopsInUse = this.laptopService.laptopsInUse;

      }

      return laptopsInUse;

        // return this.laptopService.getLaptopsInUse().pipe(
        //     first());
    }

}
