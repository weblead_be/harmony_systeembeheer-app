declare var require: any;

import { Injectable } from '@angular/core';

import { Laptop } from '../models/laptop.model';
import { LaptopsInUse } from '../models/laptops-inuse.model';

import { HttpClient, HttpParams } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LaptopService {

  public laptops: Laptop[];
  public $laptopsObs: Subject<Laptop[]> = new Subject<Laptop[]>();

  public laptopsInUse: LaptopsInUse[];
  public $laptopsInUseObs: Subject<LaptopsInUse[]> = new Subject<LaptopsInUse[]>(); 

  constructor(
    private http: HttpClient
  ) { }

  getLaptops() {
    return this.http.get<Laptop[]>(
			'http://systeembeheerapp.azurewebsites.net/api/Laptop/GetAll',
			{ observe: 'body' } );
  }

  getLaptop(id: number) {
    const laptopDetail = this.laptops.find(element => element.id === id);
    return laptopDetail;
  }

  updateLaptop(laptop: Laptop) {
    return this.http.post(
      'http://systeembeheerapp.azurewebsites.net/api/Laptop/Update', laptop
    );
  }

  addLaptop(formValue: Laptop) {
    return this.http.post<Laptop[]>(
      'http://systeembeheerapp.azurewebsites.net/api/Laptop/Add', formValue
    );
  }

  deleteLaptop(id: number) {
    let params = new HttpParams().set('id', id.toString());

    return this.http.delete(
      'http://systeembeheerapp.azurewebsites.net/api/Laptop',
      { params: params } );
  }

  deleteLaptopFromLaptopsInUse(id: number) {
    const index = this.laptopsInUse.findIndex(element => element.laptopId === id);
    if(index !== -1) {
      this.laptopsInUse.splice(index, 1);
    }
  }

  deleteUserFromLaptopsInUse(id: number) {
    const index = this.laptopsInUse.findIndex(element => element.personId === id);
    if(index !== -1) {
      this.laptopsInUse.splice(index, 1);
    }
  }

  getLaptopsInUse() {
    return this.http.get<LaptopsInUse[]>(
			'http://systeembeheerapp.azurewebsites.net/api/InUse/GetAll',
			{ observe: 'body' } );
  }

  getUseInfoByLaptop(laptopId: number) {
    const laptopObj = this.laptopsInUse.find(element => element.laptopId === laptopId);
    if(laptopObj) {
      return laptopObj;
    } else {
      return null;
    }
  }

  getLaptopInUseByUser(userid: number) {
    const laptopObj = this.laptopsInUse.find(element => element.personId === userid);

    if(laptopObj) {
      return this.laptops.find(laptop => laptop.id === laptopObj.laptopId);
    } else {
      return null;
    }
  }

  // getUseInfoByUser(userid: number) {
  //   const laptopObj = this.laptopsInUse.find(element => element.personId === userid);

  //   if(laptopObj) {
  //     return laptopObj;
  //   } else {
  //     return null;
  //   }
  // }

  // updateLaptopsInUse(formValue: Laptop) {
  //   const index = this.laptopsInUse.findIndex(element => element.laptopId === formValue.id);
  //   if(index !== -1) {
  //     this.laptopsInUse[index].laptop = formValue;
  //   }
  // }

  updateLaptopsInUse(laptopInUse: any) {
    return this.http.post(
      'http://systeembeheerapp.azurewebsites.net/api/InUse/Update', laptopInUse
    );
  }
  
  laptopsInUseCheckStatus(orgObj, newObj) {
    if(orgObj === null && (newObj.user === null || newObj.user === 'null')) {
      return 'skip';
    }
    if(orgObj === null && newObj.user) {
      return 'add';
    }
    if(orgObj && (newObj.user === null || newObj.user === 'null')) {
      return 'delete';
    }
    if(orgObj && newObj.user) {
      return 'update';
    }
  }

  updateLaptopsList(laptops: Laptop[]) {
    this.$laptopsObs.next(laptops);
    this.laptops = laptops;
  }

  updateLaptopsInUseList(laptopsInUse: LaptopsInUse[]) {
    this.$laptopsInUseObs.next(laptopsInUse);
    this.laptopsInUse = laptopsInUse;
  }

}
