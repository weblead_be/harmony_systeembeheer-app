import {Injectable} from "@angular/core";

import { first, map } from 'rxjs/operators';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable } from 'rxjs';

import { LaptopService } from './laptop.service';
import { Laptop } from '../models/laptop.model';

@Injectable()
export class LaptopResolver implements Resolve<Laptop[]> {


    constructor(private laptopService: LaptopService) {
    }

    resolve():Observable<Laptop[]> {
      let laptops;

      if(this.laptopService.laptops === undefined) {

        laptops = this.laptopService.getLaptops()
          .pipe(first())
          .pipe(map(response => {
            this.laptopService.laptops = response;
            return response;
          }));

      } else {

        laptops = this.laptopService.laptops;

      }

      return laptops;

    }

}
