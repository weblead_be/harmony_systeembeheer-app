import { Laptop } from './laptop.model';
import { Person } from 'src/app/users/models/person.model';

export class LaptopsInUse {
    id: number;
    personId: number;
    laptopId: number;
    from: Date;
    until: Date;
    note: string;
    person: Person;
    laptop: Laptop;
    name: string;
}
