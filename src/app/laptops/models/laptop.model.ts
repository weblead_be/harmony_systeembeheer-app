export enum KeyboardLayout {
  Azerty = 1,
  Qwerty = 2
}

export class Laptop {
    id: number;
    name: string;
    brand: string;
    type: string;
    serialNumber: string;
    warantyNumber: string;
    serviceTag: string;
    screenSize: string;
    keyboardLayout: KeyboardLayout;
    purchaseDate: any;
    note: string;
}
