import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdalGuard } from 'adal-angular4';

import { LaptopInuseComponent } from './laptop-inuse/laptop-inuse.component';
import { LaptopListComponent } from './laptop-list/laptop-list.component';
import { LaptopComponent } from './laptop.component';

import { LaptopInUseResolver } from './services/laptop-inuse.resolver';
import { LaptopResolver } from './services/laptop-list.resolver';
import { UserListResolver } from '../users/services/user-list.resolver';

const resolvers =  {
  laptops: LaptopResolver,
  laptopsInUse: LaptopInUseResolver,
  users: UserListResolver
}

const routes: Routes = [
    {
        path: 'laptops',
        component: LaptopComponent,
        canActivate: [AdalGuard],
        children: [
          {
            path: '',
            redirectTo: 'in-use',
            pathMatch: 'full',
            canActivate: [AdalGuard],
          },
          {
            path: 'in-use',
            component: LaptopInuseComponent,
            canActivate: [AdalGuard],
            resolve: resolvers
          },
          {
            path: 'list',
            component: LaptopListComponent,
            canActivate: [AdalGuard],
            resolve: resolvers
          }
        ]
      }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LaptopRoutingModule { }
