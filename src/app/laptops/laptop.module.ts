import { NgModule } from '@angular/core';

import { LaptopRoutingModule } from './laptop-routing.module';
import { LaptopInuseComponent } from './laptop-inuse/laptop-inuse.component';
import { LaptopDetailComponent } from './laptop-detail/laptop-detail.component';
import { LaptopAddComponent } from './laptop-add/laptop-add.component';
import { LaptopListComponent } from './laptop-list/laptop-list.component';
import { LaptopComponent } from './laptop.component';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';

import { LaptopInUseResolver } from './services/laptop-inuse.resolver';
import { LaptopResolver } from './services/laptop-list.resolver';

@NgModule({
  declarations: [
    LaptopComponent,
    LaptopListComponent,
    LaptopInuseComponent,
    LaptopListComponent,
    LaptopDetailComponent,
    LaptopAddComponent
  ],
  imports: [
    LaptopRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ],
  providers: [
    LaptopResolver,
    LaptopInUseResolver
  ],
  entryComponents: [LaptopDetailComponent, LaptopAddComponent]
})
export class LaptopModule { }
