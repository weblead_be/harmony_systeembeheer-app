import { LaptopsInUse } from './../models/laptops-inuse.model';
import { Component, OnInit } from '@angular/core';
import { LaptopService } from '../services/laptop.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-laptop-inuse',
  templateUrl: './laptop-inuse.component.html',
  styleUrls: ['./laptop-inuse.component.scss']
})
export class LaptopInuseComponent implements OnInit {

  laptopsInUse: LaptopsInUse[];

  clickableStyle = { 'cursor': 'pointer' };

  columnDefs = [
      {headerName: 'Firstname', field: 'person.firstName', sortable: true, filter: true, cellStyle: this.clickableStyle },
      {headerName: 'Lastname', field: 'person.lastName', sortable: true, filter: true, cellStyle: this.clickableStyle },
      {headerName: 'Laptop', field: 'laptop.name', sortable: true, filter: true, cellStyle: this.clickableStyle },
      {headerName: 'In use from', field: 'from', sortable: true, filter: true },
      {headerName: 'In use until', field: 'until', sortable: true, filter: true }
  ];

  constructor(
    private laptopService: LaptopService,
    private router: Router,
    private route: ActivatedRoute,
    private modal: NgbModal
  ) {
    // Prefetch api data with resolver
    route.data.subscribe(data => {
      this.laptopsInUse = data['laptopsInUse'];
      console.log(this.laptopsInUse);
    });
  }

  ngOnInit() {

  }

}
