import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-laptop',
  templateUrl: './laptop.component.html'
})
export class LaptopComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {

  }

  onAddLaptop() {
    this.router.navigate([this.router.url], { queryParams: { laptop: 'add' } });
  }



}
