import { ModalLoginComponent } from './core/modal-login/modal-login.component';
import { Component, OnInit } from '@angular/core';

import { AdalService } from 'adal-angular4';
import { environment } from 'src/environments/environment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'systeembeheer-app';

  constructor(
    private adalService: AdalService,
    private modal: NgbModal,
  ) {
    this.adalService.init(environment.adalConfig);
  }

  ngOnInit(): void {
    this.adalService.handleWindowCallback();

    console.log(window.location.origin);

    if(!this.authenticated) {
      setTimeout(() => { // setTimeout is needed for a Angular bug that throws an 'ExpressionChangedAfterItHasBeenCheckedError' error
        this.modal.open(ModalLoginComponent, {
          centered: true,
          keyboard: false,
          backdrop: 'static'
        });
      });
    }

  }



  get authenticated(): boolean {
    if (!this.adalService.userInfo.authenticated && this.adalService.userInfo.userName) {
      this.adalService.login();
    }
    return this.adalService.userInfo.authenticated;
  }

}
