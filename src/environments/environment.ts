// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,


  // For Adal configuration check this site:
  // http://www.digital-moves.eu/2018/07/19/authentication-with-azure-ad-angular-6-client-web-api/

  adalConfig: {
    tenant: 'b98d7345-3895-4772-acd5-acdb1c269ffe', //TenantId
    clientId: '4c0033c0-74d3-49dd-b697-9602347ad2a9',//ClientId guid
    redirectUri: window.location.origin + '/laptops', //
    postLogoutRedirectUri: window.location.origin + '/logout',
    endpoints: {
      'http://systeembeheerapp.azurewebsites.net/': '2d5de3f0-a3ee-49b3-85f1-b8b0fd7f9fee'//"[HOME_URL_WEB_API]": "[CLIENTID_WEB_API_GUID]"
    }
  },
  apiUrl: 'http://systeembeheerapp.azurewebsites.net/api/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
